import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_fortune_wheel/flutter_fortune_wheel.dart';

class SpinWheel extends StatefulWidget {
  @override
  _SpinWheelState createState() => _SpinWheelState();
}

class _SpinWheelState extends State<SpinWheel> {
  int selected = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xfff4f1bb),
      appBar: AppBar(
        title: Text('Random Number'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            height: 300,
            child: FortuneWheel(
              animateFirst: false,
              selected: selected,
              physics: CircularPanPhysics(
                duration: Duration(seconds: 1),
                curve: Curves.decelerate,
              ),
              onFling: () {
                setState(() {
                  selected = Random().nextInt(4);
                });
              },
              styleStrategy: UniformStyleStrategy(
                borderColor: Colors.black,
                color: Colors.orange,
                borderWidth: 5,
              ),
              items: [
                FortuneItem(child: Text("1", style: TextStyle(
                  color: Colors.white,
                ),)),
                FortuneItem(child: Text("2", style: TextStyle(
                  color: Colors.white,
                ),)),
                FortuneItem(child: Text("3", style: TextStyle(
                  color: Colors.white,
                ),)),
                FortuneItem(child: Text("4", style: TextStyle(
                  color: Colors.white,
                ),)),
                FortuneItem(child: Text("5", style: TextStyle(
                  color: Colors.white,
                ),)),
                FortuneItem(child: Text("6", style: TextStyle(
                  color: Colors.white,
                ),)),
                FortuneItem(child: Text("7", style: TextStyle(
                  color: Colors.white,
                ),)),
                FortuneItem(child: Text("8", style: TextStyle(
                  color: Colors.white,
                ),)),
                FortuneItem(child: Text("9", style: TextStyle(
                  color: Colors.white,
                ),)),
                FortuneItem(child: Text("10", style: TextStyle(
                  color: Colors.white,
                ),)),
              ],
              onAnimationEnd: () {
                print("Value : " + "$selected");
              },
            ),
          ),
          SizedBox(height: 15),
          GestureDetector(
            onTap: () {
              setState(() {
                selected = Random().nextInt(4);
              });
            },
            child: Container(
              color: Colors.blue,
              height: 40,
              width: 100,
              child: Center(child: Text("SPIN"),),
            ),
          )
        ],
      ),
    );
  }
}
