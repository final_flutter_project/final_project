import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_fortune_wheel/flutter_fortune_wheel.dart';

class SpinWheel2 extends StatefulWidget {
  @override
  _SpinWheel2State createState() => _SpinWheel2State();
}

class _SpinWheel2State extends State<SpinWheel2> {
  int selected = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xfff4f1bb),
      appBar: AppBar(
        title: Text('Random Food'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            height: 300,
            child: FortuneWheel(
              animateFirst: false,
              selected: selected,
              physics: CircularPanPhysics(
                duration: Duration(seconds: 1),
                curve: Curves.decelerate,
              ),
              onFling: () {
                setState(() {
                  selected = Random().nextInt(4);
                });
              },
              styleStrategy: UniformStyleStrategy(
                borderColor: Colors.black,
                color: Colors.orange,
                borderWidth: 5,
              ),
              items: [
                FortuneItem(child: Text("กะเพราหมูไข่ดาว", style: TextStyle(
                  color: Colors.white,
                ),)),
                FortuneItem(child: Text("ข้าวผัดอเมริกัน", style: TextStyle(
                  color: Colors.white,
                ),)),
                FortuneItem(child: Text("คะน้าหมูกรอบ", style: TextStyle(
                  color: Colors.white,
                ),)),
                FortuneItem(child: Text("ต้มยำกุ้ง", style: TextStyle(
                  color: Colors.white,
                ),)),
                FortuneItem(child: Text("ก๋วยเตี๋ยวน้ำตก", style: TextStyle(
                  color: Colors.white,
                ),)),
              ],
              onAnimationEnd: () {
                print("Value : " + "$selected");
              },
            ),
          ),
          SizedBox(height: 15),
          GestureDetector(
            onTap: () {
              setState(() {
                selected = Random().nextInt(4);
              });
            },
            child: Container(
              color: Colors.blue,
              height: 40,
              width: 100,
              child: Center(child: Text("SPIN"),),
            ),
          )
        ],
      ),
    );
  }
}
