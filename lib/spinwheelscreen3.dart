import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_fortune_wheel/flutter_fortune_wheel.dart';

class Spinwheel3 extends StatefulWidget {
  @override
  _Spinwheel3State createState() => _Spinwheel3State();
}

class _Spinwheel3State extends State<Spinwheel3> {
  int selected = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xfff4f1bb),
      appBar: AppBar(
        title: Text('Random Color'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            height: 300,
            child: FortuneWheel(
              animateFirst: false,
              selected: selected,
              physics: CircularPanPhysics(
                duration: Duration(seconds: 1),
                curve: Curves.decelerate,
              ),
              onFling: () {
                setState(() {
                  selected = Random().nextInt(4);
                });
              },
              styleStrategy: UniformStyleStrategy(
                borderColor: Colors.black,
                color: Colors.orange,
                borderWidth: 5,
              ),
              items: [
                FortuneItem(child: Text("Red", style: TextStyle(
                  color: Colors.red,
                ),)),
                FortuneItem(child: Text("Green", style: TextStyle(
                  color: Colors.green,
                ),)),
                FortuneItem(child: Text("Blue", style: TextStyle(
                  color: Colors.blue,
                ),)),
                FortuneItem(child: Text("Purple", style: TextStyle(
                  color: Colors.purple,
                ),)),
                FortuneItem(child: Text("Pink", style: TextStyle(
                  color: Colors.pink,
                ),)),
                FortuneItem(child: Text("Yellow", style: TextStyle(
                  color: Colors.yellow,
                ),)),
              ],
              onAnimationEnd: () {
                print("Value : " + "$selected");
              },
            ),
          ),
          SizedBox(height: 15),
          GestureDetector(
            onTap: () {
              setState(() {
                selected = Random().nextInt(4);
              });
            },
            child: Container(
              color: Colors.blue,
              height: 40,
              width: 100,
              child: Center(child: Text("SPIN"),),
            ),
          )
        ],
      ),
    );
  }
}
