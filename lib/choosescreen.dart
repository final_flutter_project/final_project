import 'package:flutter/material.dart';
import 'package:wheelandspin/spinwheelscreen.dart';
import 'package:wheelandspin/spinwheelscreen2.dart';
import 'package:wheelandspin/spinwheelscreen3.dart';

class ChooseScreen extends StatefulWidget {
  @override
  _ChooseScreenState createState() => _ChooseScreenState();
}

class _ChooseScreenState extends State<ChooseScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Container(
            height: 230,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(50),
              ),
              color: Color(0xff363f93),
            ),
            child: Stack(
              children: [
                Positioned(
                    top: 80,
                    left: 0,
                    child: Container(
                      height: 100,
                      width: 300,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(50),
                          bottomRight: Radius.circular(50),
                        ),
                      ),
                    )),
                Positioned(
                    top: 110,
                    left: 20,
                    child: Text(
                      "Spin Wheel",
                      style: TextStyle(fontSize: 40, color: Color(0xff363f93)),
                    ))
              ],
            ),
          ),
          SizedBox(height: 30),
          Container(
            height: 200,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(50),
                ),
                color: Colors.blue),
            child: Stack(
              children: [
              Positioned(
                  top: 80,
                  left: 0,
                  child: Container(
                    height: 100,
                    width: 300,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(50),
                        bottomRight: Radius.circular(50),
                      ),
                    ),
                  )),
              GestureDetector(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => SpinWheel()));
                },
              ),
              Positioned(
                  top: 110,
                  left: 20,
                  child: Text(
                    "Number",
                    style: TextStyle(fontSize: 40, color: Color(0xff363f93)),
                  )),
            ]),
          ),
          SizedBox(height: 30,),
          Container(
            height: 200,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(50),
                ),
                color: Colors.blue),
            child: Stack(
              children: [
              Positioned(
                  top: 80,
                  left: 0,
                  child: Container(
                    height: 100,
                    width: 300,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(50),
                        bottomRight: Radius.circular(50),
                      ),
                    ),
                  )),
              GestureDetector(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => SpinWheel2()));
                },
              ),
              Positioned(
                  top: 110,
                  left: 20,
                  child: Text(
                    "Food",
                    style: TextStyle(fontSize: 40, color: Color(0xff363f93)),
                  )),
            ]),
          ),
          SizedBox(height: 30,),
          Container(
            height: 200,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(50),
                ),
                color: Colors.blue),
            child: Stack(
              
              children: [
              Positioned(
                  top: 80,
                  left: 0,
                  child: Container(
                    height: 100,
                    width: 300,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(50),
                        bottomRight: Radius.circular(50),
                      ),
                    ),
                  )),
              GestureDetector(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Spinwheel3()));
                },
              ),
              Positioned(
                  top: 110,
                  left: 20,
                  child: Text(
                    "Color",
                    style: TextStyle(fontSize: 40, color: Color(0xff363f93)),
                  )),
            ]),
          ),
        ],
      ),
    );
  }
}
